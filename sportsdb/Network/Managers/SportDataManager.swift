//
//  SportDataManager.swift
//  sportsdb
//
//  Created by Viacheslav Bukovskyi on 28.08.2019.
//  Copyright © 2019 VB. All rights reserved.
//

import Foundation

//API: https://www.thesportsdb.com/api.php

protocol APIManager {
    typealias SportsFetchCompletionHandler = ([Sport], Error?) -> Void
    typealias LeaguesFetchCompletionHandler = ([League], Error?) -> Void
    typealias EventFetchCompletionHandler = ([Event], Error?) -> Void

    var service: NetworkService { get }

    func fetchSportKinds(completionHandler: @escaping SportsFetchCompletionHandler)
    func fetchLeagues(sportName: String, completionHandler: @escaping LeaguesFetchCompletionHandler)
    func fetchUpcomingEvents(for league: String, completionHandler: @escaping EventFetchCompletionHandler)
}

struct SportDataManager: APIManager {
    private(set) var service: NetworkService

    init(service: NetworkService) {
        self.service = service
    }

    func fetchSportKinds(completionHandler: @escaping SportsFetchCompletionHandler) {
        let request = NetworkRequest(path: "/all_sports.php", resourceType: SportKinds.self)
        request.response(using: service) { result in
            switch result {
            case .success(let sports):
                completionHandler(sports.sports, nil)
            case .failure(let error):
                completionHandler([], error)
            }
        }
    }

    func fetchLeagues(sportName: String, completionHandler: @escaping LeaguesFetchCompletionHandler) {
        let sportQueryItems = URLQueryItem(name: "s", value: sportName)
        let request = NetworkRequest(path: "/search_all_leagues.php", resourceType: Leagues.self, urlQueryItems: [sportQueryItems])
        request.response(using: service) { result in
            switch result {
            case .success(let leagues):
                completionHandler(leagues.leagues, nil)
            case .failure(let error):
                completionHandler([], error)
            }
        }
    }

    func fetchUpcomingEvents(for league: String, completionHandler: @escaping EventFetchCompletionHandler) {
        let queryItems = URLQueryItem(name: "id", value: league)
        let request = NetworkRequest(path: "/eventsnextleague.php", resourceType: Events.self, urlQueryItems: [queryItems])
        request.response(using: service) { result in
            switch result {
            case .success(let events):
                completionHandler(events.events, nil)
            case .failure(let error):
                completionHandler([], error)
            }
        }
    }
}

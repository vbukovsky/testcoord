//
//  NetworkError.swift
//  sportsdb
//
//  Created by Viacheslav Bukovskyi on 28.08.2019.
//  Copyright © 2019 VB. All rights reserved.
//

import Foundation

enum NetworkError: Int {
    case invalidURL
    case apiClient
    case serverSide

    private var userInfo: [String : Any]? {
        switch self {
        case .invalidURL:
            return [NSLocalizedDescriptionKey : "Invalid URL"]
        case .apiClient:
            return [NSLocalizedDescriptionKey : "Invalir data"]
        case .serverSide:
            return [NSLocalizedDescriptionKey : "Server side error occured"]
        }
    }

    var error: NSError {
        return NSError(domain: "error.client.api", code: rawValue, userInfo: userInfo)
    }
}

//
//  NetworkRequest.swift
//  sportsdb
//
//  Created by Viacheslav Bukovskyi on 28.08.2019.
//  Copyright © 2019 VB. All rights reserved.
//

import Foundation

struct NetworkRequest<Resource> where Resource: Decodable {
    var httpMethod: NetworkHttpMethod?
    var path: String
    var resourceType: Resource.Type
    var urlQueryItems: [URLQueryItem]?

    init(httpMethod: NetworkHttpMethod? = nil, path: String, resourceType: Resource.Type, urlQueryItems: [URLQueryItem]? = nil) {
        self.httpMethod = httpMethod
        self.path = path
        self.resourceType = resourceType
        self.urlQueryItems = urlQueryItems
    }
}

extension NetworkRequest {
    func response(using api: NetworkService, response: @escaping ((Result<Resource, Error>) -> Void)) {
        api.response(for: self, response: response)
    }
}

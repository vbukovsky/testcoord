//
//  NetworkService.swift
//  sportsdb
//
//  Created by Viacheslav Bukovskyi on 28.08.2019.
//  Copyright © 2019 VB. All rights reserved.
//

import Foundation
import UIKit

protocol NetworkService {
    func response<Resource>(for request: NetworkRequest<Resource>, response completion: @escaping ((Result<Resource, Error>) -> Void))
}

class HttpNetworkService: NetworkService {
    private let queue = DispatchQueue(label: "com.vb.networking")
    init() { }

    func response<Resource>(for request: NetworkRequest<Resource>, response completion: @escaping ((Result<Resource, Error>) -> Void)) {
        guard let url = url(for: request) else {
            let error = NetworkError.invalidURL.error
            completion(.failure(error))
            return
        }

        var urlRequest: URLRequest = .init(url: url); urlRequest.httpMethod = request.httpMethod?.rawValue
        let urlSessionConfiguration: URLSessionConfiguration = .`default`
        let urlSession: URLSession = .init(configuration: urlSessionConfiguration)
        let task = urlSession.dataTask(with: urlRequest) { [weak self] (data, response, error) in
            self?.queue.async {
                guard let data = data else {
                    let error = error ?? NetworkError.serverSide.error
                    DispatchQueue.main.async {
                        completion(.failure(error))
                    }
                    return
                }

                do {
                    let decoder: JSONDecoder = .init()
                    let resourceValue = try decoder.decode(request.resourceType, from: data)
                    DispatchQueue.main.async {
                        completion(.success(resourceValue))
                    }
                } catch let error as NSError {
                    DispatchQueue.main.async {
                        completion(.failure(error))
                    }
                } catch {
                    let error = NetworkError.apiClient.error
                    DispatchQueue.main.async {
                        completion(.failure(error))
                    }
                }
                DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
            }
        }
        task.resume()
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
    }

    private func url<Resource>(for request: NetworkRequest<Resource>) -> URL? {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "www.thesportsdb.com"
        urlComponents.path = "/api/v1/json/\(AppConstants.API.key)" + request.path
        urlComponents.queryItems = request.urlQueryItems
        return urlComponents.url
    }
}

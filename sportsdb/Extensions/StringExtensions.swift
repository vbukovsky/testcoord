//
//  StringExtensions.swift
//  sportsdb
//
//  Created by Viacheslav Bukovskyi on 8/29/19.
//  Copyright © 2019 VB. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}

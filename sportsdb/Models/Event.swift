//
//  Event.swift
//  sportsdb
//
//  Created by Viacheslav Bukovskyi on 8/29/19.
//  Copyright © 2019 VB. All rights reserved.
//

import Foundation

struct Events: Decodable {
    let events: [Event]
}

extension Events {
    enum CodingKeys: String, CodingKey {
        case events
    }

    init(from decoder: Decoder) throws {
        let rootContainer = try decoder.container(keyedBy: CodingKeys.self)
        events = (try? rootContainer.decode([Event].self, forKey: .events)) ?? []
    }
}

struct Event: Decodable {
    let id: String
    let title: String
    let league: String
    let date: String
    let time: String
}

extension Event {
    enum CodingKeys: String, CodingKey {
        case idEvent
        case strEvent
        case strLeague
        case strDate
        case strTime
    }

    init(from decoder: Decoder) throws {
        let rootContainer = try decoder.container(keyedBy: CodingKeys.self)
        id = try rootContainer.decode(String.self, forKey: .idEvent)
        title = try rootContainer.decode(String.self, forKey: .strEvent)
        league = try rootContainer.decode(String.self, forKey: .strLeague)
        date = try rootContainer.decode(String.self, forKey: .strDate)
        time = try rootContainer.decode(String.self, forKey: .strTime)
    }
}

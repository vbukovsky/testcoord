//
//  League.swift
//  sportsdb
//
//  Created by Viacheslav Bukovskyi on 8/29/19.
//  Copyright © 2019 VB. All rights reserved.
//

import Foundation

struct Leagues: Decodable {
    let leagues: [League]
}

extension Leagues {
    enum CodingKeys: String, CodingKey {
        case countrys
    }

    init(from decoder: Decoder) throws {
        let rootContainer = try decoder.container(keyedBy: CodingKeys.self)
        leagues = (try? rootContainer.decode([League].self, forKey: .countrys)) ?? []
    }
}

struct League: Decodable {
    let id: String
    let name: String
    let description: String
    let country: String
    let logoUrl: URL?
}

extension League {
    enum CodingKeys: String, CodingKey {
        case idLeague
        case strLeague
        case strDescriptionEN
        case strCountry
        case strLogo
    }

    init(from decoder: Decoder) throws {
        let rootContainer = try decoder.container(keyedBy: CodingKeys.self)
        id = try rootContainer.decode(String.self, forKey: .idLeague)
        name = try rootContainer.decode(String.self, forKey: .strLeague)
        description = (try? rootContainer.decode(String.self, forKey: .strDescriptionEN)) ?? "No description".localized
        country = try rootContainer.decode(String.self, forKey: .strCountry)
        logoUrl = try? rootContainer.decode(URL.self, forKey: .strLogo)
    }
}

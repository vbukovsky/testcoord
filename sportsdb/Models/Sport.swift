//
//  Sport.swift
//  sportsdb
//
//  Created by Viacheslav Bukovskyi on 28.08.2019.
//  Copyright © 2019 VB. All rights reserved.
//

import Foundation

struct SportKinds: Decodable {
    let sports: [Sport]
}

extension SportKinds {
    enum CodingKeys: String, CodingKey {
        case sports
    }

    init(from decoder: Decoder) throws {
        let rootContainer = try decoder.container(keyedBy: CodingKeys.self)
        sports = (try? rootContainer.decode([Sport].self, forKey: .sports)) ?? []
    }
}

struct Sport: Decodable {
    let id: String
    let name: String
    let thumbUrl: URL
    let description: String
}

extension Sport {
    enum CodingKeys: String, CodingKey {
        case idSport
        case strSport
        case strSportThumb
        case strSportDescription
    }

    init(from decoder: Decoder) throws {
        let rootContainer = try decoder.container(keyedBy: CodingKeys.self)
        id = try rootContainer.decode(String.self, forKey: .idSport)
        name = try rootContainer.decode(String.self, forKey: .strSport)
        thumbUrl = try rootContainer.decode(URL.self, forKey: .strSportThumb)
        description = try rootContainer.decode(String.self, forKey: .strSportDescription)
    }
}

extension Sport: Equatable {
    static func == (lhs: Sport, rhs: Sport) -> Bool {
        return
            lhs.id == rhs.id &&
            lhs.name == rhs.name &&
            lhs.thumbUrl == rhs.thumbUrl &&
            lhs.description == rhs.description
    }
}

//
//  SportsViewModel.swift
//  sportsdb
//
//  Created by Viacheslav Bukovskyi on 8/30/19.
//  Copyright © 2019 VB. All rights reserved.
//

import Foundation

class SportsViewModel {
    private var sports: [Sport] = []
    private var networkManager: APIManager

    var onDataUpdated: (() -> Void)?
    var onDataLoadingError: ((Error) -> Void)?

    init(networkManager: APIManager) {
        self.networkManager = networkManager
    }

    var title: String {
        return "Sports".localized
    }

    var numberOfRecords: Int {
        return sports.count
    }

    func sportViewModel(at index: Int) -> SportViewModel {
        return SportViewModel(sport: sports[index])
    }

    func sport(at index: Int) -> Sport {
        return sports[index]
    }

    func loadData() {
        networkManager.fetchSportKinds { [weak self] sportTypes, error in
            guard error == nil else {
                self?.onDataLoadingError?(error!)
                return
            }
            self?.sports = sportTypes
            self?.onDataUpdated?()
        }
    }
}



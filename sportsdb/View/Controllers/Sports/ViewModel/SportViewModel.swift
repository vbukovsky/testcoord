//
//  SportViewModel.swift
//  sportsdb
//
//  Created by Viacheslav Bukovskyi on 8/30/19.
//  Copyright © 2019 VB. All rights reserved.
//

import Foundation

struct SportViewModel: Equatable {
    private let sport: Sport

    init(sport: Sport) {
        self.sport = sport
    }

    var imageUrl: URL {
        return sport.thumbUrl
    }

    var title: String {
        return sport.name
    }

    static func == (lhs: SportViewModel, rhs: SportViewModel) -> Bool {
        return lhs.sport == rhs.sport
    }
}

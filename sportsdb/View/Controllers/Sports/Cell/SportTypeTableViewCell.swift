//
//  SportTypeTableViewCell.swift
//  sportsdb
//
//  Created by Viacheslav Bukovskyi on 8/30/19.
//  Copyright © 2019 VB. All rights reserved.
//

import UIKit
import SDWebImage

class SportTypeTableViewCell: UITableViewCell {

    @IBOutlet weak var thumbImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    func setupUI(with viewModel: SportViewModel) {
        thumbImageView.sd_setImage(with: viewModel.imageUrl)
        titleLabel.text = viewModel.title
    }
}

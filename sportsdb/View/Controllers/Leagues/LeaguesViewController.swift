//
//  LeaguesViewController.swift
//  sportsdb
//
//  Created by Viacheslav Bukovskyi on 29.08.2019.
//  Copyright © 2019 VB. All rights reserved.
//

import UIKit

class LeaguesViewController: UIViewController {
    private struct Config {
        static let cellIdentifier = "LeagueCellIdentifier"
        static let rowHeight: CGFloat = 100
    }
    private var viewModel: LeaguesViewModel
    weak var coordinator: LeaguesCoordinatable?

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!

    init(viewModel: LeaguesViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "LeaguesViewController", bundle: Bundle.main)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupViewModel()
        setLoading(true)
        setupUI()

        viewModel.loadData()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        coordinator?.onNavigatingBack?()
    }

    func setLoading(_ isLoading: Bool) {
        activityIndicator.isHidden = !isLoading
        tableView.isHidden = isLoading
    }

    private func setupViewModel() {
        viewModel.onDataUpdated = { [weak self] in
            self?.setLoading(false)
            self?.tableView.reloadData()
        }

        viewModel.onDataLoadingError = { _ in
            // error handler should go here
        }
    }

    private func setupUI() {
        title = viewModel.title
        tableView.tableHeaderView = UIView()
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = Config.rowHeight

        tableView.register(UINib(nibName: String(describing: LeagueTableViewCell.self), bundle: Bundle.main), forCellReuseIdentifier: Config.cellIdentifier)
    }
}

extension LeaguesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRecords
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Config.cellIdentifier, for: indexPath) as? LeagueTableViewCell else { return UITableViewCell() }
        let cellViewModel = viewModel.leagueViewModel(at: indexPath.row)
        cell.setupUI(with: cellViewModel)

        return cell
    }
}

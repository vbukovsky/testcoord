//
//  LeagueTableViewCell.swift
//  sportsdb
//
//  Created by Viacheslav Bukovskyi on 8/30/19.
//  Copyright © 2019 VB. All rights reserved.
//

import UIKit
import SDWebImage

class LeagueTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!

    func setupUI(with viewModel: LeagueViewModel) {
        titleLabel.text = viewModel.name
        countryLabel.text = viewModel.country
        logoImageView.sd_setImage(with: viewModel.logoUrl, placeholderImage: #imageLiteral(resourceName: "logo"))
    }
}

//
//  LeagueViewModel.swift
//  sportsdb
//
//  Created by Viacheslav Bukovskyi on 8/30/19.
//  Copyright © 2019 VB. All rights reserved.
//

import Foundation

struct LeagueViewModel {
    private let league: League

    init(league: League) {
        self.league = league
    }

    var name: String {
        return league.name
    }

    var country: String {
        return league.country
    }

    var logoUrl: URL? {
        return league.logoUrl
    }
}


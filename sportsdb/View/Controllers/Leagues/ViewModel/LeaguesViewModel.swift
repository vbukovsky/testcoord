//
//  LeaguesViewModel.swift
//  sportsdb
//
//  Created by Viacheslav Bukovskyi on 8/30/19.
//  Copyright © 2019 VB. All rights reserved.
//

import Foundation

class LeaguesViewModel {
    private var sportName: String
    private var networkManager: APIManager

    private var leagues: [League] = []

    var onDataUpdated: (() -> Void)?
    var onDataLoadingError: ((Error) -> Void)?

    var title: String {
        return sportName
    }

    var numberOfRecords: Int {
        return leagues.count
    }

    func leagueViewModel(at index: Int) -> LeagueViewModel {
        return LeagueViewModel(league: leagues[index])
    }
    init(sportName: String, networkManager: APIManager) {
        self.sportName = sportName
        self.networkManager = networkManager
    }

    func loadData() {
        networkManager.fetchLeagues(sportName: sportName) { [weak self] leagues, error in
            guard error == nil else {
                self?.onDataLoadingError?(error!)
                return
            }
            self?.leagues = leagues
            self?.onDataUpdated?()
        }
    }
}

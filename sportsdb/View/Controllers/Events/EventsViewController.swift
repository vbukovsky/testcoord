//
//  EventsViewController.swift
//  sportsdb
//
//  Created by Viacheslav Bukovskyi on 29.08.2019.
//  Copyright © 2019 VB. All rights reserved.
//

import UIKit

class EventsViewController: UIViewController {

    weak var coordinator: EventsCoordinatable?

    init() {
        super.init(nibName: "EventsViewController", bundle: Bundle.main)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func goBackTapped(_ sender: Any) {
        coordinator?.onNavigatingBack?()
    }

    @IBAction func goHomeTapped(_ sender: Any) {
        coordinator?.onNavigatingHome?()
    }
}

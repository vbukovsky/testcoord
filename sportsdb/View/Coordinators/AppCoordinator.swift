//
//  AppCoordinator.swift
//  sportsdb
//
//  Created by Viacheslav Bukovskyi on 8/30/19.
//  Copyright © 2019 VB. All rights reserved.
//

import UIKit

protocol AppCoordinatable: Coordinator { }

final class AppCoordinator: AppCoordinatable {
    var router: Router = PushRouter()
    var childCoordinators: [Coordinator] = []
    var isCompleted: (() -> ())?

    var apiManager: APIManager = {
        let service = HttpNetworkService()
        return SportDataManager(service: service)
    }()

    let window : UIWindow

    init(window: UIWindow) {
        self.window = window
    }

    func start() {
        let sportsCoordinator = SportsCoordinator(router: router, networkingManager: apiManager)

        // store child coordinator
        self.store(coordinator: sportsCoordinator)
        sportsCoordinator.start()

        window.rootViewController = router.navigationController
        window.makeKeyAndVisible()
    }
}

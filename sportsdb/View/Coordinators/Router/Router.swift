//
//  Router.swift
//  sportsdb
//
//  Created by Viacheslav Bukovskyi on 8/30/19.
//  Copyright © 2019 VB. All rights reserved.
//

import Foundation
import UIKit

protocol Router: class {
    var navigationController: UINavigationController { get set }
    func show(_ viewController: UIViewController, animated: Bool)
    func navigateBack(animated: Bool)
    func navigateBackToRoot(animated: Bool)
}

extension Router {
    func show(_ viewController: UIViewController) {
        show(viewController, animated: true)
    }

    func navigateBack() {
        navigateBack(animated: true)
    }

    func navigateBackToRoot() {
        navigateBackToRoot(animated: true)
    }
}

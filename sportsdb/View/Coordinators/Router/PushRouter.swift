//
//  PushRouter.swift
//  sportsdb
//
//  Created by Viacheslav Bukovskyi on 8/30/19.
//  Copyright © 2019 VB. All rights reserved.
//

import UIKit

class PushRouter: Router {
    var navigationController: UINavigationController

    init(navigationController: UINavigationController = UINavigationController()) {
        self.navigationController = navigationController
    }

    func show(_ viewController: UIViewController, animated: Bool) {
        navigationController.pushViewController(viewController, animated: animated)
    }

    func navigateBack(animated: Bool) {
        navigationController.popViewController(animated: animated)
    }

    func navigateBackToRoot(animated: Bool) {
        navigationController.popToRootViewController(animated: animated)
    }
}

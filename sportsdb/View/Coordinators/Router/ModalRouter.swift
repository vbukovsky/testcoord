//
//  ModalRouter.swift
//  sportsdb
//
//  Created by Viacheslav Bukovskyi on 8/30/19.
//  Copyright © 2019 VB. All rights reserved.
//

import UIKit

class ModalRouter: Router {
    var navigationController: UINavigationController

    init(navigationController: UINavigationController = UINavigationController()) {
        self.navigationController = navigationController
    }

    func show(_ viewController: UIViewController, animated: Bool) {
        navigationController.present(viewController, animated: animated)
    }

    func navigateBack(animated: Bool) {
        navigationController.dismiss(animated: animated)
    }

    func navigateBackToRoot(animated: Bool) {
        navigationController.dismiss(animated: animated)
    }
}

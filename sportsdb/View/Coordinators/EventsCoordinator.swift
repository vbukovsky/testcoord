//
//  EventsCoordinator.swift
//  sportsdb
//
//  Created by Viacheslav Bukovskyi on 8/30/19.
//  Copyright © 2019 VB. All rights reserved.
//

import Foundation

protocol EventsCoordinatable: Coordinator {
    var onNavigatingBack: (() -> Void)? { get set }
    var onNavigatingHome: (() -> Void)? { get set }
}

final class EventsCoordinator: EventsCoordinatable {
    var childCoordinators = [Coordinator]()
    var router: Router
    var onNavigatingBack: (() -> Void)?
    var onNavigatingHome: (() -> Void)?

    init(router: Router) {
        self.router = router
    }

    func start() {
        let viewController = EventsViewController()
        viewController.coordinator = self
        router.show(viewController, animated: true)
    }
}

//
//  LeaguesCoordinator.swift
//  sportsdb
//
//  Created by Viacheslav Bukovskyi on 8/30/19.
//  Copyright © 2019 VB. All rights reserved.
//

import Foundation

protocol LeaguesCoordinatable: Coordinator {
    func showEvents(for league: League)
    var onNavigatingBack: (() -> Void)? { get set }
}

final class LeaguesCoordinator: LeaguesCoordinatable {
    var childCoordinators = [Coordinator]()
    var router: Router
    var networkingManager: APIManager

    private var sport: Sport

    var onNavigatingBack: (() -> Void)?
    var onNavigatingHome: (() -> Void)?

    init(sport: Sport, router: Router, networkingManager: APIManager) {
        self.sport = sport
        self.router = router
        self.networkingManager = networkingManager
    }

    func start() {
        let viewModel = LeaguesViewModel(sportName: sport.name, networkManager: networkingManager)
        let viewController = LeaguesViewController(viewModel: viewModel)
        viewController.coordinator = self
        router.show(viewController, animated: true)
    }

    func showEvents(for league: League) {
        let eventsCoordinator = EventsCoordinator(router: router)
        eventsCoordinator.start()

        store(coordinator: eventsCoordinator)

        eventsCoordinator.onNavigatingBack = { [weak self] in
            self?.router.navigateBack()
            self?.release(coordinator: eventsCoordinator)
        }
    }
}

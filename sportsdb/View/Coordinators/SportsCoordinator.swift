//
//  SportsCoordinator.swift
//  sportsdb
//
//  Created by Viacheslav Bukovskyi on 29.08.2019.
//  Copyright © 2019 VB. All rights reserved.
//

import Foundation
import UIKit

protocol SportsCoordinatable: Coordinator {
    func showLeagues(for sport: Sport)
}

final class SportsCoordinator: SportsCoordinatable {
    var childCoordinators = [Coordinator]()
    var router: Router
    var networkingManager: APIManager

    init(router: Router, networkingManager: APIManager) {
        self.router = router
        self.networkingManager = networkingManager
    }

    func start() {
        let viewModel = SportsViewModel(networkManager: networkingManager)
        let viewController = SportsViewController(viewModel: viewModel)
        viewController.coordinator = self
        router.show(viewController, animated: false)
    }

    func showLeagues(for sport: Sport) {
        let leaguesCoordinator = LeaguesCoordinator(sport: sport, router: router, networkingManager: networkingManager)
        leaguesCoordinator.start()

        store(coordinator: leaguesCoordinator)

        leaguesCoordinator.onNavigatingBack = { [weak self] in
            self?.router.navigateBack()
            self?.release(coordinator: leaguesCoordinator)
        }

        leaguesCoordinator.onNavigatingHome = { [weak self] in
            self?.router.navigateBackToRoot()
            self?.release(coordinator: leaguesCoordinator)
        }
    }
}

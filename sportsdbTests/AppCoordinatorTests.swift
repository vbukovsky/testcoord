//
//  AppCoordinatorTests.swift
//  AppCoordinatorTests
//
//  Created by Viacheslav Bukovskyi on 28.08.2019.
//  Copyright © 2019 VB. All rights reserved.
//

import XCTest
import Nimble
@testable import sportsdb

class AppCoordinatorTests: XCTestCase {

    var appCoordinator: AppCoordinator!
    override func setUp() {
        let window = UIWindow()
        appCoordinator = AppCoordinator(window: window)
        appCoordinator.apiManager = MockApiManager(service: FakeApiService())
    }

    override func tearDown() {
        appCoordinator = nil
    }

    func testRouterTriggeredOnStart() {
        let router = MockRouter()
        appCoordinator.router = router
        appCoordinator.start()


        expect(router.lastAction?.action).to(equal(.showViewController))
    }

    func testChildCoordinatorsRetained() {
        appCoordinator.start()
        let childCoordinators = appCoordinator.childCoordinators
        expect(childCoordinators).toNot(beEmpty())
    }

    func testCorrectRedirect() {
        appCoordinator.start()
        let isSportListCoordinator = appCoordinator.childCoordinators.first is SportsCoordinator

        expect(isSportListCoordinator).to(beTrue())
    }
}

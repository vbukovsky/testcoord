//
//  MockRouter.swift
//  sportsdbTests
//
//  Created by Viacheslav Bukovskyi on 8/30/19.
//  Copyright © 2019 VB. All rights reserved.
//

import Foundation
import UIKit
@testable import sportsdb

class MockRouter: Router {
    enum Action {
        case showViewController
        case popViewController
        case popToRootViewController
    }
    typealias RouterAction = (action: Action, animated: Bool)

    var lastAction: RouterAction?

    var navigationController: UINavigationController

    init(navigationController: UINavigationController = UINavigationController()) {
        self.navigationController = navigationController
    }

    func show(_ viewController: UIViewController, animated: Bool) {
        lastAction = RouterAction(.showViewController, animated)
    }

    func navigateBack(animated: Bool) {
        lastAction = RouterAction(.popViewController, animated)
    }

    func navigateBackToRoot(animated: Bool) {
        lastAction = RouterAction(.popToRootViewController, animated)
    }
}

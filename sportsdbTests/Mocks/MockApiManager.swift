//
//  MockApiManager.swift
//  sportsdbTests
//
//  Created by Viacheslav Bukovskyi on 8/30/19.
//  Copyright © 2019 VB. All rights reserved.
//

import Foundation
@testable import sportsdb

class MockApiManager: APIManager {

    var fetchedSportList: Bool = false
    var fetchedLeagues: Bool = false
    var fetchedEvents: Bool = false

    private(set) var service: NetworkService

    init(service: NetworkService) {
        self.service = service
    }

    func fetchSportKinds(completionHandler: @escaping SportsFetchCompletionHandler) {
        fetchedSportList = true
    }

    func fetchLeagues(sportName: String, completionHandler: @escaping LeaguesFetchCompletionHandler) {
        fetchedLeagues = true
    }

    func fetchUpcomingEvents(for league: String, completionHandler: @escaping EventFetchCompletionHandler) {
        fetchedEvents = true
    }
}

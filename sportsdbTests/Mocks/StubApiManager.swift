//
//  StubApiManager.swift
//  sportsdbTests
//
//  Created by Viacheslav Bukovskyi on 30.08.2019.
//  Copyright © 2019 VB. All rights reserved.
//

import UIKit
@testable import sportsdb

class StubApiManager: APIManager {
    private(set) var service: NetworkService

    init(service: NetworkService) {
        self.service = service
    }

    func fetchSportKinds(completionHandler: @escaping SportsFetchCompletionHandler) {
        let sport1 = Sport(id: "sport1id", name: "sport1name", thumbUrl: URL(string: "http://qwe.qwe")!, description: "sport1description")
        let sport2 = Sport(id: "sport2id", name: "sport2name", thumbUrl: URL(string: "http://qwe.qwe")!, description: "sport2description")
        let sport3 = Sport(id: "sport3id", name: "sport3name", thumbUrl: URL(string: "http://qwe.qwe")!, description: "sport3description")

        completionHandler([sport1, sport2, sport3], nil)
    }

    func fetchLeagues(sportName: String, completionHandler: @escaping LeaguesFetchCompletionHandler) {
        let league1 = League(id: "league1id", name: "league1name", description: "league1description", country: "league1country", logoUrl: URL(string: ""))
        let league2 = League(id: "league2id", name: "league2name", description: "league2description", country: "league2country", logoUrl: URL(string: ""))
        let league3 = League(id: "league3id", name: "league3name", description: "league3description", country: "league3country", logoUrl: URL(string: ""))
        completionHandler([league1, league2, league3], nil)
    }

    func fetchUpcomingEvents(for league: String, completionHandler: @escaping EventFetchCompletionHandler) {
    }
}

//
//  FakeApiService.swift
//  sportsdbTests
//
//  Created by Viacheslav Bukovskyi on 8/30/19.
//  Copyright © 2019 VB. All rights reserved.
//

import Foundation
@testable import sportsdb

class FakeApiService: NetworkService {
    func response<Resource>(for request: NetworkRequest<Resource>, response completion: @escaping ((Result<Resource, Error>) -> Void)) where Resource : Decodable {
    }
}

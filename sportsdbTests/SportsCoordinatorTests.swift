//
//  SportsCoordinatorTests.swift
//  sportsdbTests
//
//  Created by Viacheslav Bukovskyi on 30.08.2019.
//  Copyright © 2019 VB. All rights reserved.
//

import XCTest
import Nimble
@testable import sportsdb

class SportsCoordinatorTests: XCTestCase {

    var coordinator: SportsCoordinator!
    var router: MockRouter!
    var networkManager: MockApiManager!

    override func setUp() {

        router = MockRouter()
        networkManager = MockApiManager(service: FakeApiService())
        coordinator = SportsCoordinator(router: router, networkingManager: networkManager)
    }

    override func tearDown() {
        coordinator = nil
        networkManager = nil
        router = nil
    }

    func testInitRouterSet() {
        expect(self.coordinator.router).toNot(beNil())
    }

    func testInitNetworkingManagerSet() {
        expect(self.coordinator.networkingManager).toNot(beNil())
    }

    func testRouterTriggeredOnStart() {
        coordinator.start()
        expect(self.router.lastAction?.action).to(equal(.showViewController))
    }

    func testRouterTriggeredOnStartWithoutAnimation() {
        coordinator.start()
        expect(self.router.lastAction?.animated).to(beFalse())
    }

    func testShowLaegues() {
        let sport = Sport(id: "sportId", name: "sport name", thumbUrl: URL(string: "http://test.com")!, description: "description")
        coordinator.showLeagues(for: sport)

        expect(self.router.lastAction?.action).to(equal(.showViewController))
    }

    func testLeaguesShownAnimated() {
        let sport = Sport(id: "sportId", name: "sport name", thumbUrl: URL(string: "http://test.com")!, description: "description")
        coordinator.showLeagues(for: sport)

        expect(self.router.lastAction?.animated).to(beTrue())
    }

    func testLeaguesCoordinatorIsStored() {
        let sport = Sport(id: "sportId", name: "sport name", thumbUrl: URL(string: "http://test.com")!, description: "description")
        coordinator.showLeagues(for: sport)

        let lastStored = coordinator.childCoordinators.last as? LeaguesCoordinatable
        expect(lastStored).toNot(beNil())
    }
}

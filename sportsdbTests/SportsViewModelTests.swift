//
//  SportsViewModelTests.swift
//  sportsdbTests
//
//  Created by Viacheslav Bukovskyi on 30.08.2019.
//  Copyright © 2019 VB. All rights reserved.
//

import XCTest
import Nimble
@testable import sportsdb

class SportsViewModelTests: XCTestCase {

    var viewModel: SportsViewModel!

    override func setUp() {
        let service = FakeApiService()
        let manager = StubApiManager(service: service)

        viewModel = SportsViewModel(networkManager: manager)
    }

    override func tearDown() {
        viewModel = nil
    }

    func testLoadedDataIsStored() {
        viewModel.loadData()
        expect(self.viewModel.numberOfRecords).to(beGreaterThan(0))
    }

    func testNumberOfRecords() {
        viewModel.loadData()
        expect(self.viewModel.numberOfRecords).to(equal(3))
    }

    func testGetSportAtIndex() {
        viewModel.loadData()
        expect(self.viewModel.sport(at: 0).id).to(equal("sport1id"))
        expect(self.viewModel.sport(at: 1).id).to(equal("sport2id"))
        expect(self.viewModel.sport(at: 2).id).to(equal("sport3id"))
    }

    func testViewModelByIndex() {
        let sport = Sport(id: "sport2id", name: "sport2name", thumbUrl: URL(string: "http://qwe.qwe")!, description: "sport2description")
        viewModel.loadData()
        expect(self.viewModel.sportViewModel(at: 1)).to(equal(SportViewModel(sport: sport)))
    }
}
